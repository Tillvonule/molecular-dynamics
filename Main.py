import numpy as np
# from matplotlib import pyplot as plt
# from mpl_toolkits.mplot3d import Axes3D  # noqa: F401 unused import
# import imageio as io
from Konstanten import *
import copy
import scipy.stats as st


class boltzmann_pdf(st.rv_continuous):
    def __init__(self, temp, mass, name, a=0, b=np.inf):
        st.rv_continuous.__init__(self, name=name, a=a, b=b)
        self.temp = temp
        self.mass = mass
    def _pdf(self,x):
        return (4*np.pi*(self.mass/(np.pi*2*Boltzmann_Konstant*self.temp))**(3/2))*x**2*np.exp(-(self.mass*x**2/(2*Boltzmann_Konstant*self.temp)))

class ParticleSphere:
    def __init__(self, n, radius):
        '''
        :param n: amount of particles in box
        :param radius: radius of sphere containing the particles on its surface
        '''
        self.n = n
        self.radius = radius
        self.particles = np.random.rand(n, 4)
        self.particles[:, 0] *= 2*np.pi
        self.particles[:, 1] *= np.pi
        self.hits = None
        self.timestep = None

    def whatsintheBOX(self):
        return "{} particles".format(self.n)

    def distance(self, i, j):
        '''
        calculating the arc-distance between to particles on the sphere
        :param i: index of particle
        :param j: index of particle
        :return: arc-distance on the sphere
        '''
        p1 = np.array([self.radius*np.sin(self.particles[i][1])*np.cos(self.particles[i][0]),
                       self.radius*np.sin(self.particles[i][1])*np.sin(self.particles[i][0]),
                       self.radius*np.cos(self.particles[i][1])])
        p2 = np.array([self.radius * np.sin(self.particles[j][1]) * np.cos(self.particles[j][0]),
                       self.radius * np.sin(self.particles[j][1]) * np.sin(self.particles[j][0]),
                       self.radius * np.cos(self.particles[j][1])])
        dist = np.arccos(np.dot(p1, p2)/(self.radius**2))*self.radius
        return dist

    def direction(self, i, j):
        """
        gives normalized direction between particle i and j
        """
        d = self.particles[i][:2]-self.particles[j][:2]
        d[0] %= np.pi*2
        d[1] %= np.pi
        return d


class MolecularDyn:
    """
    :param sphere: current positions of all particles
    :param last_sphere: previous positions of all particles
    :param sigma: distance parameter of the lennard jones potential
    :param epsilon: energy parameter  of the lennard jones potential
    :param mass: mass of particle
    """
    def __init__(self, n, r, sigma, epsilon, mass=0.0001):
        self.sphere = ParticleSphere(n, r)
        self.last_sphere = None
        self.sigma = sigma
        self.epsilon = epsilon
        self.mass = mass

    def force(self, particle_id, sphere):
        """calculates force from the negative gradient of the lennard jones potential
        :param particle_id:
        :param sphere:
        """
        force = np.zeros(2)
        for i in range(sphere.n):
            if i == particle_id:
                continue
            dist = sphere.distance(particle_id, i)
            direction = sphere.direction(particle_id, i)
            force += -4*self.epsilon*(-12*self.sigma**12/(dist**13) +
                                      6*self.sigma**6/(dist**7))*direction
        return force

    def do_time_step(self, dt):
        """
        :param dt: discrete timestep
        propagates a sphere of particles using verlet(original)
        """
        next_sphere = copy.deepcopy(self.sphere)
        for p1 in range(self.sphere.n):
            f = self.force(p1, self.sphere)
            a = f/self.mass
            new_pos = self.sphere.particles[p1][:2] + \
                      dt**2*a + \
                      self.sphere.particles[p1][2:]*dt
            next_sphere.particles[p1][2:] = (self.sphere.particles[p1][:2]-new_pos)/dt
            next_sphere.particles[p1][:2] = new_pos
        self.sphere = next_sphere

    def get_temp(self):
        v = 0
        for p in self.sphere.particles:
            v += abs(p[2:])
        return self.mass*(np.dot(v/self.sphere.n, v/self.sphere.n))/(2*Boltzmann_Konstant)

    def hit_or_nohit(self, mu, t, deltat):
        if np.random.rand() < mu*np.exp(-mu*t)*deltat:
            return True
        else:
            return False

    def iterate_over(self, timesteps, dt, temperature=None, mu=1):
        """
        :param timesteps:
        :param dt: discrete timestep for do_time_step method
        :return:
        """
        self.sphere.hits = np.zeros((self.sphere.n, timesteps))
        self.timestep = dt
        if temperature:
            my_cv = boltzmann_pdf(name='boltzmann_pdf', temp=temperature, mass=self.mass)
            self.sphere.hits = np.zeros((self.sphere.n, timesteps))
            for i in range(self.sphere.n):
                last_hit = 0
                for t_index in range(timesteps):
                    if self.hit_or_nohit(mu, t_index*dt, t_index*dt-last_hit):
                        last_hit = t_index*dt
                        self.sphere.hits[i][t_index] = my_cv.rvs()
            for i in range(timesteps):
                for j in range(self.sphere.n):
                    if not self.sphere.hits[j][i] == 0:
                        self.sphere.particles[j][2:] = self.sphere.particles[j][2:]/np.linalg.norm(self.sphere.particles[j][2:])*self.sphere.hits[j][i]
                self.do_time_step(dt)
        else:
            for i in range(timesteps):
                self.do_time_step(dt)


if __name__ == "__main__":
    md = MolecularDyn(n=100, r=1, sigma=0.2, epsilon=0.1)
    t_list = []
    md.iterate_over(20, 0.01)
    t_list.append(md.get_temp())


    ###############
    # Plot sphere #
    ###############

    # Create a sphere
    # r = 1
    # pi = np.pi
    # cos = np.cos
    # sin = np.sin
    # phi, theta = np.mgrid[0.0:pi:100j, 0.0:2.0 * pi:100j]
    # x = r * sin(phi) * cos(theta)
    # y = r * sin(phi) * sin(theta)
    # z = r * cos(phi)
    #
    # fig = plt.figure()
    # for i in range(100):
    #     # ax = fig.add_subplot(1, 1, 1, projection='3d')
    #     ax = fig.add_subplot(111)
    #     x = (md.sphere.particles[:, 0] % np.pi) * np.cos(md.sphere.particles[:, 1])
    #     y = md.sphere.particles[:, 1] % (2*np.pi)
    #     ax.plot([-np.pi, -np.pi, np.pi, np.pi, -np.pi], [0, 2*np.pi, 2*np.pi, 0, 0])
    #     ax.plot(x, y, '.')
    #     ax.set_xlim([-np.pi, np.pi])
    #     ax.set_ylim([0, 2*np.pi])
    #
    #     # ax.plot_surface(
    #     #     x, y, z, rstride=1, cstride=1, color='c', alpha=0.6, linewidth=0)
    #     #
    #     # ax.scatter(md.sphere.radius * np.sin(md.sphere.particles[:, 1]) * np.cos(md.sphere.particles[:, 0]),
    #     #            md.sphere.radius * np.sin(md.sphere.particles[:, 1]) * np.sin(md.sphere.particles[:, 0]),
    #     #            md.sphere.radius * np.cos(md.sphere.particles[:, 1]),
    #     #            s=20)
    #     # ax.set_xlim([-1, 1])
    #     # ax.set_ylim([-1, 1])
    #     # ax.set_zlim([-1, 1])
    #     # plt.tight_layout()
    #     plt.savefig("images/{}.png".format(i))
    #     plt.clf()
    #     md.do_time_step(0.1)
    #
    # images = []
    # for i in range(100):
    #     images.append(io.imread("images/{}.png".format(i)))
    # io.mimsave('surface1.gif', images, duration=0.5)
